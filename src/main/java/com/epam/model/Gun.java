package com.epam.model;

import java.util.ArrayList;
import java.util.List;

public class Gun {
    private int gunID;
    private String model;
    private String handy;
    private String origin;
    private List<Material> materials = new ArrayList<Material>();
    private TacticalSpecifications specifications;
    public Gun(){

    }

    public int getGunID() {

        return gunID;
    }

    public void setGunID(int gunID) {
        this.gunID = gunID;
    }

    public String getModel(String model) {
        return this.model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getHandy() {
        return handy;
    }

    public void setHandy(String handy) {
        this.handy = handy;
    }

    public String getOrigin(String origin) {
        return this.origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public List<Material> getMaterials(Material material) {
        return materials;
    }

    public void setMaterials(List<Material> materials) {
        this.materials = materials;
    }

    public TacticalSpecifications getSpecifications(TacticalSpecifications specifications) {
        return this.specifications;
    }

    public void setSpecifications(TacticalSpecifications specifications) {
        this.specifications = specifications;
    }

    @Override
    public String toString() {
        return "Gun:" +
                "||gunID:" + gunID +
                "||model:" + model +
                "||handy:" + handy  +
                "||origin:" + origin  +
                "||materials:" + materials +
                "||specifications:" + specifications + "\n";
    }
}
