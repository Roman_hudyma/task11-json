package com.epam.model;

public class Material {
    private String material;

    public Material() {
    }

    public Material(String material) {
        this.material = material;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    @Override
    public String toString() {
        return "Material:" +
                "material:" + getMaterial();
    }
}
