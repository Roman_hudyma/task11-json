package com.epam.model;

public class Guns {

    private Gun[] guns;

    public Guns() {

    }

    public Gun[] getGuns() {
        return guns;
    }

    public void setGuns(Gun[] guns) {
        this.guns = guns;
    }
}
