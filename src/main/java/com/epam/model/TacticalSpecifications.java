package com.epam.model;

public class TacticalSpecifications {
    private String carry;
    private String sightingRange;
    private Boolean presenceOfTheClamp;
    private Boolean availabilityOptics;

    public String getCarry() {
        return carry;
    }

    public void setCarry(String carry) {
        this.carry = carry;
    }

    public String getSightingRange() {
        return sightingRange;
    }

    public void setSightingRange(String sightingRange) {
        this.sightingRange = sightingRange;
    }

    public Boolean getPresenceOfTheClamp() {
        return presenceOfTheClamp;
    }

    public void setPresenceOfTheClamp(Boolean presenceOfTheClamp) {
        this.presenceOfTheClamp = presenceOfTheClamp;
    }

    public Boolean getAvailabilityOptics() {
        return availabilityOptics;
    }

    public void setAvailabilityOptics(Boolean availabilityOptics) {
        this.availabilityOptics = availabilityOptics;
    }

    @Override
    public String toString() {
        return "TacticalSpecifications:" +
                "||carry:" + carry +
                "||sightingRange:" + sightingRange  +
                "||presenceOfTheClamp:" + presenceOfTheClamp +
                "||availabilityOptics:" + availabilityOptics +
                "}\n";
    }
}
