package com.epam.view;

import com.epam.controller.GsonHandler;
import com.epam.controller.JacksonHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Scanner;

public class ApplicationView {
    private static ResourceBundle messages = ResourceBundle.getBundle("command");
    private static List<String> menu = new ArrayList<>();
    private static Scanner scanner = new Scanner(System.in);
    private int choose;

    public void run() {
        createMenu();
        do {
            for (String s : menu) {
                System.out.println(s);
            }
            choose = scanner.nextInt();
            switch (choose) {
                case 1:
                    new GsonHandler().run();
                    break;
                case 2:
                    new JacksonHandler().run();
                    break;
            }
        }
        while (!(choose == 3));
    }

    private static void createMenu() {
        menu.add((messages.getString("command.1")));
        menu.add((messages.getString("command.2")));

    }
}
