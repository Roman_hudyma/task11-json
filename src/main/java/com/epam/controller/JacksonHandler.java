package com.epam.controller;

import com.epam.model.Gun;
import com.epam.model.Guns;
import com.epam.model.JsonValidator;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class JacksonHandler {
    private static final Logger logger = LogManager.getLogger();

    public void run() {
        try {
            if (JsonValidator.validate("validator.json", "guns.json")) {
                Guns ammunition = read("guns.json");
                List<Gun> candyList = Arrays.asList(ammunition.getGuns());
                candyList.forEach(System.out::println);
            }
        } catch (IOException e) {
            logger.error(e);
        }
    }

    public Guns read(String path) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(new File(path), Guns.class);
    }
}
