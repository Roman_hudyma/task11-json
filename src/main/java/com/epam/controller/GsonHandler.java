package com.epam.controller;

import com.epam.model.Gun;
import com.epam.model.Guns;
import com.epam.model.JsonValidator;
import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;
import java.util.List;

public class GsonHandler {
    private static final Logger logger = LogManager.getLogger();

    public static void main(String[] args) {
        new GsonHandler().run();
    }

    public void run(){
        try {
            if(JsonValidator.validate("validator.json", "guns.json")) {
                Guns ammunition= read("guns.json");
                List<Gun> guns = Arrays.asList(ammunition.getGuns());
                guns.forEach(System.out::println);
            }
        } catch (FileNotFoundException e) {
            logger.error(e);
        }

    }

    public Guns read(String path) throws FileNotFoundException {
        Gson gson = new Gson();
        return gson.fromJson(new FileReader(path), Guns.class);
    }
}
