package com.epam;

import com.epam.view.ApplicationView;

public class ApplicationRunner {
    public static void main(String[] args) {
        new ApplicationView().run();
    }
}
